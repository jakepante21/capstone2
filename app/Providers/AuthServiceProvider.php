<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use App\Product;
use App\Category;
use App\Transaction;
use App\Policies\ProductPolicy;
use App\Policies\CategoryPolicy;
use App\Policies\TransactionPolicy;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
        Product::class => ProductPolicy::class,
        Category::class => CategoryPolicy::class,
        Transaction::class => TransactionPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define("isAdmin",function($user){
            return $user->role_id === 1;
        });

        Gate::define("isLogged",function($user){
            return $user->role_id !== null;
        });

        Gate::define("isUser",function($user){
            return $user->role_id === 2;
        });
    }
}
