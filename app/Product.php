<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function category(){
    	return $this->belongsTo("App\Category");
    }
    public function vehicle(){
    	return $this->hasMany("App\Vehicle");
    }
    public function stock_status(){
    	return $this->belongsTo("App\StockStatus");
    }
    public function assettransactions(){
    	return $this->hasMany("App\AssetTransaction");
    }
}
