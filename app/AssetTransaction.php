<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssetTransaction extends Model
{
    public function product(){
    	return $this->belongsTo("App\Product");
    }
    public function vehicle(){
    	return $this->hasMany("App\Vehicle");
    }
}
