<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    public function product(){
    	return $this->belongsTo("App\Product");
    }
    public function asset_statuses()
	{
		return $this->belongsTo('App\AssetStatus');
	}
	public function assettransactions(){
    	return $this->belongsTo("App\AssetTransaction");
    }
}
