<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Request extends Model
{
    public function status()
	{
		return $this->belongsTo('App\RequestStatus');
	}

	public function payment_mode()
	{
		return $this->belongsTo('App\PaymentMode');
	}

	public function user()
	{
		return $this->belongsTo('App\User');
	}

	public function products()
	{
		return $this->belongsToMany('App\Product','asset_transaction')
		->withPivot('subtotal','price');
	}
}
