<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    public function status()
	{
		return $this->belongsTo('App\RequestStatus');
	}

	public function payment_mode()
	{
		return $this->belongsTo('App\PaymentMode');
	}

	public function user()
	{
		return $this->belongsTo('App\User');
	}

	public function vehicles()
	{
		return $this->belongsToMany('App\Product','asset_transactions')
		->withPivot('subtotal','price','days',"product_id","return_date","date_needed","vehicle_code_number","id");
	}
}
