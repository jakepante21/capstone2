<?php

namespace App\Http\Controllers;

use App\Transaction;
use App\User;
use App\Product;
use App\Vehicle;
use App\RequestStatus;
use App\AssetTransaction;
use Illuminate\Http\Request;
use Str;
use Auth;
use Session;
use DB;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // if(Auth::user()->role_id == 1){
        //     $transactions = Transaction::all();
        // }
        // else{
        //     $transactions = Transaction::all()->whereIn("user_id",Auth::user()->id);
        // }
        
        if(Auth::user()->role_id == 1){
            $transactions = Transaction::all();
        }
        else{
            $transactions = Transaction::all()->whereIn("user_id",Auth::user()->id);
        }
        
        $asset_transactions = AssetTransaction::all();
        $pending = DB::table("transactions")->select("id",DB::raw("count(*) as total"))->groupBy("id")->where("status_id",1)->get();
        $accepted = DB::table("transactions")->select("id",DB::raw("count(*) as total"))->groupBy("id")->where("status_id",2)->get();
        $rejected = DB::table("transactions")->select("id",DB::raw("count(*) as total"))->groupBy("id")->where("status_id",3)->get();
        $completed = DB::table("transactions")->select("id",DB::raw("count(*) as total"))->groupBy("id")->where("status_id",4)->get();
        $cancelled = DB::table("transactions")->select("id",DB::raw("count(*) as total"))->groupBy("id")->where("status_id",5)->get();
        $statuses = RequestStatus::all();
        return view("transactions.index")->with("transactions",$transactions)->with("statuses",$statuses)->with("assettransactions",$asset_transactions)->with("pendings",$pending)->with("accepteds",$accepted)->with("rejecteds",$rejected)->with("completeds",$completed)->with("cancelleds",$cancelled);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $total = 0;
        $product_ids = array_keys(Session::get("cart"));
        $products = Product::find($product_ids);
        $vehicles = DB::table("vehicles")->where("product_id",$product_ids)->get();
        $transaction = new Transaction;
        $transaction->user_id = Auth::user()->id;
        $transaction->request_code = Auth::user()->id . Str::random(10);
        foreach($products as $product){
            
            
            $product->days = Session::get("cart.$product->id");
            $product->subtotal = $product->days * $product->price;
            $product->date_needed = Session::get("cart-date.$product->id");
            $product->return_date = Session::get("cart-return.$product->id");
            $total += $product->subtotal;
            $transaction->save();
            
                
                $transaction->vehicles()->attach($product->id,[
                "days" => $product->days,
                "subtotal" => $product->subtotal,
                "date_needed" => $product->date_needed,
                "return_date" => $product->return_date,
                "price" => $product->price]);
            
            
        }

        $transaction->total = $total;
        $transaction->save();

        Session::forget("cart");
        Session::forget("cart-date");
        Session::forget("cart-return");

        return redirect(route("transactions.show",["transaction" => $transaction->id]));

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $transaction)
    {
        $this->authorize("view",$transaction);
        $statuses = RequestStatus::all();
        return view("transactions.show")->with("transaction",$transaction)->with("statuses",$statuses);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaction $transaction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaction $transaction)
    {
        
        // foreach($transaction->vehicles as $j){
        //     dd($asset_transac = DB::table("asset_transactions")->whereIn("product_id",[$j->pivot->product_id])->get());
        // }
        if($request->set == "accept"){
            $transaction->status_id = 2;
            $transaction->save();
            $asset_tras = AssetTransaction::all();
            $c = [];
            foreach($asset_tras as $asset_tra){
                if($asset_tra->transaction_id == $transaction->id){
                    $asset_transac = DB::table("asset_transactions")->where("transaction_id",$asset_tra->transaction_id)->whereIn("product_id",[$asset_tra->product_id])->get();
                    foreach($asset_transac as $ass){
                        $vehicles = DB::table("vehicles")->where("asset_status_id",1)->where("product_id",$ass->product_id)->get();
                        $vehicless = Vehicle::all();
                        $random = $vehicles->random();
            
                            if($asset_tra->vehicle_code_number == null){
                                if($asset_tra->transaction_id == $transaction->id){
                                    $asset_tra->vehicle_code_number = $random->asset_code_number;
                                    $asset_tra->save();
                                }
                            }
                            
                        

                        foreach($vehicless as $vehicled){
                            if($vehicled->id == $random->id){
                                $vehicled->asset_status_id = 3;
                                $vehicled->save();
                            }
                        }
                    }
                }
            }
            // dd($asset_transac);
            // dd($asset_transac = DB::table("asset_transactions")->whereIn("product_id",$c)->get());
            // foreach($asset_transac as $a){
            //     $d = $a->product_id;
            // }
            // dd($vehicles = DB::table("vehicles")->where("asset_status_id",1)->where("product_id",$d)->get());
            // dd($vehicles);
            // $vehicless = Vehicle::all();
            // $random = $vehicles->random();
            // $assets = AssetTransaction::all();

            // foreach($assets as $asset){
            //     if($asset->transaction_id == $transaction->id){
            //         $asset->vehicle_code_number = $random->asset_code_number;
            //         $asset->save();
            //     }
            // }

            // foreach($vehicless as $vehicled){
            //     if($vehicled->id == $random->id){
            //         $vehicled->asset_status_id = 3;
            //         $vehicled->save();
            //     }
            // }
        }
        else if($request->set == "reject"){
            $transaction->status_id = 3;
            $transaction->save();
        }
        else if($request->set == "complete"){
            $transaction->status_id = 4;
            $transaction->save();
            $asset_tras = AssetTransaction::all();
            $vehiclesss = Vehicle::all();
            foreach($asset_tras as $asset_tra){
                    foreach($vehiclesss as $vec){
                        if($asset_tra->vehicle_code_number == $vec->asset_code_number){
                            
                            $vec->asset_status_id = 1;
                            $vec->save();
                        }
                    }
                
                }
            }
        else if($request->set == "cancel"){
            $transaction->status_id = 5;
            $transaction->save();
        }
        
        

        return redirect(route("transactions.index"));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaction $transaction)
    {
        //
    }

    public function action(Request $request, AssetTransaction $asset)
    {
        $transactions = Transaction::all();

        if($request->set == "accept"){
            $c = 0;
            foreach($transactions as $transaction){
                if($transaction->id == $asset->transaction_id){
                    $transaction->status_id = 2;
                    $transaction->save();
                }
            }
            $vehicles = DB::table("vehicles")->where("asset_status_id",1)->where("product_id",$asset->product_id)->get();

            $vehicless = Vehicle::all();
            $random = $vehicles->random();
            $asset->vehicle_code_number = $random->asset_code_number;
            $asset->save();
            

            foreach($vehicless as $vehicled){
                if($vehicled->id == $random->id){
                    $vehicled->asset_status_id = 3;
                    $vehicled->save();
                }
            }
        }
        else if($request->set == "reject"){
            foreach($transactions as $transaction){
                if($transaction->id == $asset->transaction_id){
                    $transaction->status_id = 3;
                    $transaction->save();
                }
            }
        }
        else if($request->set == "complete"){
            foreach($transactions as $transaction){
                if($transaction->id == $asset->transaction_id){
                    $transaction->status_id = 4;
                    $transaction->save();
                }
            }
            $vehicless = Vehicle::all();
            foreach($vehicless as $vehicled){
                if($vehicled->asset_code_number == $request->assett){
                    $vehicled->asset_status_id = 1;
                    $vehicled->save();
                }
            }
        }
        return redirect(route("transactions.index"));
    

    }
}
