<?php

namespace App\Http\Controllers;

use App\Vehicle;
use App\Product;
use App\Category;
use App\AssetStatus;
use DB;
use Session;
use Illuminate\Http\Request;

class VehicleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Vehicle $vehicle)
    {
        $this->authorize("view",$vehicle);
        $vehicles = Vehicle::all();
        $products = Product::all();
        $stock_count = Vehicle::count("id");
       
        if($stock_count > 0){
            $count_per_product = DB::table("vehicles")->select('product_id', DB::raw('count(*) as total'))->groupBy("product_id")->get();
            $count_avail = DB::table("vehicles")->select('product_id', DB::raw('count(*) as total'))->groupBy("product_id")->where("asset_status_id",1)->get();
            $not = ["asset_status_id" => 2,"asset_status_id" => 3];
            $count_not = DB::table("vehicles")->select('product_id', DB::raw('count(*) as total'))->groupBy("product_id")->where("asset_status_id",2)->orWhere("asset_status_id",3)->get();
            // dd(DB::table("vehicles")->select('product_id', DB::raw('count(*) as total'))->groupBy("product_id")->where("asset_status_id",2)->orWhere("asset_status_id",3)->get());
            // dd(count($count_not));
            if(count($count_avail) > 0){
                foreach($products as $product){   
                    foreach($count_avail as $avail){
                        if($product->id == $avail->product_id){
                            $result = Product::find($product->id);
                            $result->stocks_avail = $avail->total;
                            $result->save();

                            break;
                        }

                        if($product->id !== $avail->product_id){
                            $result = Product::find($product->id);
                            $result->stocks_avail = 0;
                            $result->save();

                        }
                    }    
                }
            }
            else{
                foreach($products as $product){
                    $product->stocks_avail = 0;
                    $product->save();
                }
            }

            if(count($count_not) > 0){
                foreach($products as $product){   
                    foreach($count_not as $nott){
                        if($product->id == $nott->product_id){
                            $result = Product::find($product->id);
                            $result->stocks_not_avail = $nott->total;
                            $result->save();

                            break;
                        }

                        if($product->id !== $nott->product_id){
                            $result = Product::find($product->id);
                            $result->stocks_not_avail = 0;
                            $result->save();

                        }
                    }    
                }
            }else{
                foreach($products as $product){
                    $product->stocks_not_avail = 0;
                    $product->save();
                }
            }
            if(count($count_per_product) > 0){
                foreach($products as $product){   
                    foreach($count_per_product as $countd){
                        if($product->id == $countd->product_id){
                            $result = Product::find($product->id);
                            $result->stocks = $countd->total;
                        // echo $product->id;
                            $result->save();
                        // $product->stocks = $countd->total;
                        // $product->save();
                        // echo $countd->product_id;
                        // echo $countd->total;
                            break;
                        }

                        if($product->id !== $countd->product_id){
                        // echo $product->id;
                            $result = Product::find($product->id);
                            $result->stocks = 0;
                            $result->save();

                        }



                // else if($product->id !== $countd->product_id){ 
                //     $product->stocks = 0;
                //     $product->save();
                //     // echo $product->id;
                // }
                // else{
                //     $product->stocks = 0;
                //     $product->save();
                //     // echo $product->id;
                // }
                    }    
                }
            }

        }
        else{
            foreach($products as $product){
                $product->stocks = 0;
                $product->stocks_avail = 0;
                $product->stocks_not_avail = 0;
                $product->save();
            }
        }

        
        
        $vehicles = Vehicle::all();
        $products = Product::all();
        foreach($products as $product){
            if($product->stocks <= 0){
                $product->stock_status_id = 3;
                $product->save();
            }
            else if($product->stocks_avail <= 0){
                $product->stock_status_id = 2;
                $product->save();
            }
            else{
                $product->stock_status_id = 1;
                $product->save();
            }
            // if($product->stocks_avail <= 0){
            //     $product->stock_status_id = 2;
            //     $product->save();
            // }
            // else if($product->stocks_avail > 0){
            //     $product->stock_status_id = 1;
            //     $product->save();
            // }
            // else{
            //     $product->stock_status_id = 3;
            //     $product->save();
            // }
        }
        return view("vehicles.index")->with("vehicles",Vehicle::all())->with("stock",$stock_count)->with("products",$products)->with("statuses",AssetStatus::all());
    //     $a = 0;
    //     foreach($countss as $countd){
    //         $a++;
    //         echo $a;
    //     }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Product $product)
    {
        if(Session::has("product")){
            $result = Session::get("product");
            $product = Product::find($result);
            return view("vehicles.create")->with("product",$product)->with("categories",Category::all());
        }else{
            return "no session";
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "plate-number" => "required|unique:vehicles,plate_number"
        ]);

        $vehicles = Vehicle::latest("asset_code_number")->first();

        // dd(isset($vehicles));

       if(isset($vehicles)){
            $codes = Vehicle::latest("asset_code_number")->first();
            $asset_number = $codes->asset_code_number;
            $asset_number++;
       }
       else{
            $code_array = [0,0,1];
            
            if($code_array[2] > 9){
                $code_array[2] = 0;
                $code_array[1] += 1;
            }

            $asset_number = "VHCL" . $code_array[0] . $code_array[1] . $code_array[2];
       }
        
        
        $product = Session::get("product");
        // dd($product);
        // dd($asset_number = "VHCL" . $code_array[0] . $code_array[1] . $code_array[2]);
        $vehicle = new Vehicle;
        $vehicle->plate_number = $request->input("plate-number");
        $vehicle->asset_code_number = $asset_number;
        $vehicle->product_id = $product;
        $vehicle->save();  

        return redirect(route("vehicles.index"));
        
        Session::forget("product");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function show(Vehicle $vehicle)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function edit(Vehicle $vehicle)
    {
        return dd($vehicle);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vehicle $vehicle)
    {
        $asset_status = $request->status;
        $vehicle->asset_status_id = $asset_status;
        $vehicle->save();

        return redirect(route("vehicles.index"));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vehicle $vehicle)
    {
        $vehicle->delete();
        return redirect(route("vehicles.index")); 
    }

    public function pass(Request $request, $vehicle)
    {
        $request->session()->put("product",$vehicle);
        return redirect(route("vehicles.create"));
    }
}
