<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Product;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Product $product)
    {
        $this->authorize("view",$product);
        if(Session::has("cart")){
            $product_ids = array_keys(Session::get("cart"));
            $products = Product::find($product_ids);
            $total = 0;
            foreach($products as $product){
                $product->days = Session::get("cart.$product->id");
                $product->date = Session::get("cart-date.$product->id");
                for($x = 1;$x <= $product->days; $x++){
                    $product->date++;
                    $product->returndate = $product->date;
                }
                session()->put("cart-return.$product->id",$product->returndate);
                $product->date = Session::get("cart-date.$product->id");
                $product->subtotal = $product->days * $product->price;
                $total += $product->subtotal;
            }

            return view("carts.index")->with("products",$products)->with("total",$total);
        }else{
            return view("carts.index");
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            "days" => "required|min:1|max:5"
       ]);
        $request->validate([
            "date" => "required|date"
       ]);

       // $request->date++;
       // echo $request->date;
       $days = $request->days;
       $date = $request->date;
       // for($x = 1;$x <= $days; $x++){
       //      $date++;
       // }
       // echo $date;
       $request->session()->put("cart.$id",$days);
       $request->session()->put("cart-date.$id",$date);
       // dd( $request->session()->get("cart"));
       return redirect(route("carts.index"));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $request->session()->forget("cart.$id");
        if (count($request->session()->get("cart")) == 0){
            $request->session()->forget("cart");
        }
        return redirect(route("carts.index"))->with("status","Product removed from the cart");
    }

    public function empty(){
        Session::forget("cart");
        return redirect(route("carts.index"))->with("status","Cart is cleared");
    }
}

