<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get("/vehicles/vehicles/{vehicle}","VehicleController@pass")->name("vehicles.pass");
Route::put("/transaction/transaction/{asset}","TransactionController@action")->name("transactions.action");
Route::get("/products/products/{product}","ProductController@auto_update")->name("products.auto");
Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::delete("/carts/empty","CartController@empty")->name("carts.empty");


Route::get('/home', 'HomeController@index')->name('home');
Route::resource("categories","CategoryController");
Route::resource("products","ProductController");
Route::resource("carts","CartController");
Route::resource("vehicles","VehicleController");
Route::resource("transactions","TransactionController");