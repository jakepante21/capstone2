@extends("layouts.app")
@section("content")
<div class="container-fluid">
    <div class="jumbotron">
        <h2 class="text-center">Requests</h2>
    </div>
    <ul class="nav nav-pills mb-3" id="list-tab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="list-all-list" data-toggle="list" href="#list-all" role="tab" aria-controls="all" aria-selected="true">All</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="list-pending-list" data-toggle="list" href="#list-accepted" role="tab" aria-controls="accepted" aria-selected="false">Accepted</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="list-pending-list" data-toggle="list" href="#list-pending" role="tab" aria-controls="pending" aria-selected="false">Pending</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="list-completed-list" data-toggle="list" href="#list-completed" role="tab" aria-controls="completed" aria-selected="false">Completed</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="list-rejected-list" data-toggle="list" href="#list-rejected" role="tab" aria-controls="rejected" aria-selected="false">Rejected</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="list-cancelled-list" data-toggle="list" href="#list-cancelled" role="tab" aria-controls="cancelled" aria-selected="false">Cancelled</a>
        </li>
    </ul>
    <div class="tab-content" id="pills-tabContent">
        <div class="tab-pane fade show active" id="list-all" role="tabpanel" aria-labelledby="list-all-list">
            <div class="row">
                <div class="col-12 col-md-8 col-lg-12">
                    {{-- start of table --}}
                    <div class="table-responsive">
                        <table class="table table-striped table-hover text-center">
                            {{-- start of thead --}}
                            <thead>
                                <th>Request Code</th>
                                <th>Asset Code No.</th>
                                <th>Product Name</th>
                                <th>No. of Days</th>
                                <th>Date Needed</th>
                                <th>Return Date</th>
                                <th>Status</th>
                                <th>Action</th>
                                <th>User</th>
                            </thead>
                            {{-- end of thead --}}
                            <tbody>
                                @foreach($transactions as $transaction)
                                @if(count($transaction->vehicles) == 1)
                                <tr>
                                    <th>{{$transaction->request_code}}</th>
                                    @foreach($transaction->vehicles as $wow)
                                    @if($wow->pivot->vehicle_code_number == null)
                                    <td><h6>Not Set</h6></td>
                                    @else
                                    <td><h6>{{$wow->pivot->vehicle_code_number}}</h6></td>
                                    @endif
                                    <td><h6>{{$wow->name}}</h6></td>
                                    <td><h6>{{$wow->pivot->days}}</h6></td>
                                    <td><h6>{{date("F d, Y",strtotime($wow->pivot->date_needed))}}</h6></td>
                                    <td><h6>{{date("F d, Y",strtotime($wow->pivot->return_date))}}</h6></td>
                                    @endforeach
                                    <td>
                                        <h5>
                                        @if($transaction->status->id == 1)
                                        <span class="badge badge-warning">{{$transaction->status->name}}</span>
                                        </h5>
                                        @elseif($transaction->status->id == 2)
                                        <span class="badge badge-primary">{{$transaction->status->name}}</span>
                                        </h5>
                                        @elseif($transaction->status->id == 3)
                                        <span class="badge badge-danger">{{$transaction->status->name}}</span>
                                        </h5>
                                        @elseif($transaction->status->id == 4)
                                        <span class="badge badge-primary">{{$transaction->status->name}}</span>
                                        </h5>
                                        @elseif($transaction->status->id == 5)
                                        <span class="badge badge-secondary">{{$transaction->status->name}}</span>
                                        </h5>
                                        @endif
                                    </td>
                                    @can("isUser")
                                    @if($transaction->status->id == 5)
                                    <td><a href="{{route("transactions.show",["transaction" => $transaction->id])}}" class="btn btn-primary">Show Details</a></td>
                                    @else
                                    <td>
                                        <form action="{{route("transactions.update",["transaction" => $transaction->id])}}?set=cancel" method="post">
                                            @csrf
                                            @method("PUT")
                                            <button class="btn btn-danger">Cancel</button>
                                        </form>
                                    </td>
                                    @endif
                                    
                                    @endcan
                                    @can("isAdmin")
                                    <td>
                                    @if($transaction->status_id == 2)
                                        <form action="{{route("transactions.update",["transaction" => $transaction->id])}}?set=complete" method="post">
                                            @csrf
                                            @method("PUT")
                                            
                                            <button class="btn btn-primary">Complete</button>
                                        </form>
                                    @elseif($transaction->status_id == 3)
                                    <h5><span class="badge badge-danger">{{$transaction->status->name}}</span></h5>
                                    @elseif($transaction->status_id == 4)
                                    @elseif($transaction->status_id == 5)
                                        
                                    @else
                                        <form action="{{route("transactions.update",["transaction" => $transaction->id])}}?set=accept" method="post">
                                            @csrf
                                            @method("PUT")
                                        <button class="btn btn-primary mb-1">Accept</button>
                                        </form>
                                        <form action="{{route("transactions.update",["transaction" => $transaction->id])}}?set=reject" method="post">
                                            @csrf
                                            @method("PUT")
                                        <button class="btn btn-danger">Reject</button>
                                        </form>
                                    @endif
                                    </td>
                                    @endcan
                                    <td><h6>{{$transaction->user->name}}</h6></td>


                                </tr>
                                @else
                                <tr>
                                    <th>{{$transaction->request_code}}</th>
                                    <td>
                                    @foreach($transaction->vehicles as $wow)
                                    @if($wow->pivot->vehicle_code_number == null)
                                    <h6>Not Set</h6>
                                    @else
                                    <h6>{{$wow->pivot->vehicle_code_number}}</h6>
                                    @endif
                                    @endforeach
                                    </td>
                                    <td>
                                        @foreach($transaction->vehicles as $wow)
                                        <h6>{{$wow->name}}</h6>
                                        @endforeach
                                    </td>
                                    <td>
                                        @foreach($transaction->vehicles as $wow)
                                        <h6>{{$wow->pivot->days}}</h6>
                                        @endforeach
                                    </td>
                                    <td>
                                        @foreach($transaction->vehicles as $wow)
                                        <h6>{{date("F d, Y",strtotime($wow->pivot->date_needed))}}</h6>
                                        @endforeach
                                    </td>
                                    <td>
                                        @foreach($transaction->vehicles as $wow)
                                        <h6>{{date("F d, Y",strtotime($wow->pivot->return_date))}}</h6>
                                        @endforeach
                                    </td>
                                    <td>
                                        <h5>
                                        @if($transaction->status->id == 1)
                                        <span class="badge badge-warning">{{$transaction->status->name}}</span>
                                        </h5>
                                        @elseif($transaction->status->id == 2)
                                        <span class="badge badge-primary">{{$transaction->status->name}}</span>
                                        </h5>
                                        @elseif($transaction->status->id == 3)
                                        <span class="badge badge-danger">{{$transaction->status->name}}</span>
                                        </h5>
                                        @elseif($transaction->status->id == 4)
                                        <span class="badge badge-primary">{{$transaction->status->name}}</span>
                                        </h5>
                                        @elseif($transaction->status->id == 5)
                                        <span class="badge badge-secondary">{{$transaction->status->name}}</span>
                                        </h5>
                                        @endif
                                    </td>
                                    @can("isUser")
                                    @if($transaction->status->id == 5)
                                    <td><a href="{{route("transactions.show",["transaction" => $transaction->id])}}" class="btn btn-primary">Show Details</a></td>
                                    @else
                                    <td>
                                        <form action="{{route("transactions.update",["transaction" => $transaction->id])}}?set=cancel" method="post">
                                            @csrf
                                            @method("PUT")
                                            <button class="btn btn-danger">Cancel</button>
                                        </form>
                                    </td>
                                    @endif
                                    
                                    @endcan
                                    @can("isAdmin")
                                    <td>
                                    @if($transaction->status_id == 2)
                                        <form action="{{route("transactions.update",["transaction" => $transaction->id])}}?set=complete" method="post">
                                            @csrf
                                            @method("PUT")
                                            
                                            <button class="btn btn-primary">Complete</button>
                                        </form>
                                    @elseif($transaction->status_id == 3)
                                    <h5><span class="badge badge-danger">{{$transaction->status->name}}</span></h5>
                                    @elseif($transaction->status_id == 4)
                                    @elseif($transaction->status_id == 5)
                                    @else
                                        <form action="{{route("transactions.update",["transaction" => $transaction->id])}}?set=accept" method="post">
                                            @csrf
                                            @method("PUT")
                                        <button class="btn btn-primary mb-1">Accept</button>
                                        </form>
                                        <form action="{{route("transactions.update",["transaction" => $transaction->id])}}?set=reject" method="post">
                                            @csrf
                                            @method("PUT")
                                        <button class="btn btn-danger">Reject</button>
                                        </form>
                                    @endif
                                    </td>
                                    @endcan
                                    <td><h6>{{$transaction->user->name}}</h6></td>
                                </tr>
                                @endif
                                

                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="list-accepted" role="tabpanel" aria-labelledby="list-accepted-list">
            <div class="row">
                <div class="col-12 col-md-8 col-lg-12">
                    {{-- start of table --}}
                    <div class="table-responsive">
                        <table class="table table-striped table-hover text-center">
                            {{-- start of thead --}}
                            <thead>
                                <th>Request Code</th>
                                <th>Asset Code No.</th>
                                <th>Product Name</th>
                                <th>No. of Days</th>
                                <th>Date Needed</th>
                                <th>Return Date</th>
                                <th>Status</th>
                                <th>Action</th>
                                <th>User</th>
                            </thead>
                            {{-- end of thead --}}
                            <tbody>
                                @foreach($accepteds as $accepted)
                                @foreach($transactions as $transaction)
                                @if($accepted ->id == $transaction->id)
                                @if(count($transaction->vehicles) == 1)
                                <tr>
                                    <th>{{$transaction->request_code}}</th>
                                    @foreach($transaction->vehicles as $wow)
                                    @if($wow->pivot->vehicle_code_number == null)
                                    <td><h6>Not Set</h6></td>
                                    @else
                                    <td><h6>{{$wow->pivot->vehicle_code_number}}</h6></td>
                                    @endif
                                    <td><h6>{{$wow->name}}</h6></td>
                                    <td><h6>{{$wow->pivot->days}}</h6></td>
                                    <td><h6>{{date("F d, Y",strtotime($wow->pivot->date_needed))}}</h6></td>
                                    <td><h6>{{date("F d, Y",strtotime($wow->pivot->return_date))}}</h6></td>
                                    @endforeach
                                    <td>
                                        <h5>
                                        @if($transaction->status->id == 1)
                                        <span class="badge badge-warning">{{$transaction->status->name}}</span>
                                        </h5>
                                        @elseif($transaction->status->id == 2)
                                        <span class="badge badge-primary">{{$transaction->status->name}}</span>
                                        </h5>
                                        @elseif($transaction->status->id == 3)
                                        <span class="badge badge-danger">{{$transaction->status->name}}</span>
                                        </h5>
                                        @elseif($transaction->status->id == 4)
                                        <span class="badge badge-primary">{{$transaction->status->name}}</span>
                                        </h5>
                                        @elseif($transaction->status->id == 5)
                                        <span class="badge badge-secondary">{{$transaction->status->name}}</span>
                                        </h5>
                                        @endif
                                    </td>
                                    @can("isUser")
                                    @if($transaction->status->id == 5)
                                    <td><a href="{{route("transactions.show",["transaction" => $transaction->id])}}" class="btn btn-primary">Show Details</a></td>
                                    @else
                                    <td>
                                        <form action="{{route("transactions.update",["transaction" => $transaction->id])}}?set=cancel" method="post">
                                            @csrf
                                            @method("PUT")
                                            <button class="btn btn-danger">Cancel</button>
                                        </form>
                                    </td>
                                    @endif
                                    
                                    @endcan
                                    @can("isAdmin")
                                    <td>
                                    @if($transaction->status_id == 2)
                                        <form action="{{route("transactions.update",["transaction" => $transaction->id])}}?set=complete" method="post">
                                            @csrf
                                            @method("PUT")
                                            
                                            <button class="btn btn-primary">Complete</button>
                                        </form>
                                    @elseif($transaction->status_id == 3)
                                    <h5><span class="badge badge-danger">{{$transaction->status->name}}</span></h5>
                                    @elseif($transaction->status_id == 4)
                                    @elseif($transaction->status_id == 5)
                                    @else
                                        <form action="{{route("transactions.update",["transaction" => $transaction->id])}}?set=accept" method="post">
                                            @csrf
                                            @method("PUT")
                                        <button class="btn btn-primary mb-1">Accept</button>
                                        </form>
                                        <form action="{{route("transactions.update",["transaction" => $transaction->id])}}?set=reject" method="post">
                                            @csrf
                                            @method("PUT")
                                        <button class="btn btn-danger">Reject</button>
                                        </form>
                                    @endif
                                    </td>
                                    @endcan
                                    <td><h6>{{$transaction->user->name}}</h6></td>


                                </tr>
                                @else
                                <tr>
                                    <th>{{$transaction->request_code}}</th>
                                    <td>
                                    @foreach($transaction->vehicles as $wow)
                                    @if($wow->pivot->vehicle_code_number == null)
                                    <h6>Not Set</h6>
                                    @else
                                    <h6>{{$wow->pivot->vehicle_code_number}}</h6>
                                    @endif
                                    @endforeach
                                    </td>
                                    <td>
                                        @foreach($transaction->vehicles as $wow)
                                        <h6>{{$wow->name}}</h6>
                                        @endforeach
                                    </td>
                                    <td>
                                        @foreach($transaction->vehicles as $wow)
                                        <h6>{{$wow->pivot->days}}</h6>
                                        @endforeach
                                    </td>
                                    <td>
                                        @foreach($transaction->vehicles as $wow)
                                        <h6>{{date("F d, Y",strtotime($wow->pivot->date_needed))}}</h6>
                                        @endforeach
                                    </td>
                                    <td>
                                        @foreach($transaction->vehicles as $wow)
                                        <h6>{{date("F d, Y",strtotime($wow->pivot->return_date))}}</h6>
                                        @endforeach
                                    </td>
                                    <td>
                                        <h5>
                                        @if($transaction->status->id == 1)
                                        <span class="badge badge-warning">{{$transaction->status->name}}</span>
                                        </h5>
                                        @elseif($transaction->status->id == 2)
                                        <span class="badge badge-primary">{{$transaction->status->name}}</span>
                                        </h5>
                                        @elseif($transaction->status->id == 3)
                                        <span class="badge badge-danger">{{$transaction->status->name}}</span>
                                        </h5>
                                        @elseif($transaction->status->id == 4)
                                        <span class="badge badge-primary">{{$transaction->status->name}}</span>
                                        </h5>
                                        @elseif($transaction->status->id == 5)
                                        <span class="badge badge-secondary">{{$transaction->status->name}}</span>
                                        </h5>
                                        @endif
                                    </td>
                                    @can("isUser")
                                    @if($transaction->status->id == 5)
                                    <td><a href="{{route("transactions.show",["transaction" => $transaction->id])}}" class="btn btn-primary">Show Details</a></td>
                                    @else
                                    <td>
                                        <form action="{{route("transactions.update",["transaction" => $transaction->id])}}?set=cancel" method="post">
                                            @csrf
                                            @method("PUT")
                                            <button class="btn btn-danger">Cancel</button>
                                        </form>
                                    </td>
                                    @endif
                                    
                                    @endcan
                                    @can("isAdmin")
                                    <td>
                                    @if($transaction->status_id == 2)
                                        <form action="{{route("transactions.update",["transaction" => $transaction->id])}}?set=complete" method="post">
                                            @csrf
                                            @method("PUT")
                                            
                                            <button class="btn btn-primary">Complete</button>
                                        </form>
                                    @elseif($transaction->status_id == 3)
                                    <h5><span class="badge badge-danger">{{$transaction->status->name}}</span></h5>
                                    @elseif($transaction->status_id == 4)
                                    @elseif($transaction->status_id == 5)
                                    @else
                                        <form action="{{route("transactions.update",["transaction" => $transaction->id])}}?set=accept" method="post">
                                            @csrf
                                            @method("PUT")
                                        <button class="btn btn-primary mb-1">Accept</button>
                                        </form>
                                        <form action="{{route("transactions.update",["transaction" => $transaction->id])}}?set=reject" method="post">
                                            @csrf
                                            @method("PUT")
                                        <button class="btn btn-danger">Reject</button>
                                        </form>
                                    @endif
                                    </td>
                                    @endcan
                                    <td><h6>{{$transaction->user->name}}</h6></td>
                                </tr>
                                @endif
                                @endif
                                @endforeach
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="list-pending" role="tabpanel" aria-labelledby="list-pending-list">
            <div class="row">
                <div class="col-12 col-md-8 col-lg-12">
                    {{-- start of table --}}
                    <div class="table-responsive">
                        <table class="table table-striped table-hover text-center">
                            {{-- start of thead --}}
                            <thead>
                                <th>Request Code</th>
                                <th>Asset Code No.</th>
                                <th>Product Name</th>
                                <th>No. of Days</th>
                                <th>Date Needed</th>
                                <th>Return Date</th>
                                <th>Status</th>
                                <th>Action</th>
                                <th>User</th>
                            </thead>
                            {{-- end of thead --}}
                            <tbody>
                                @foreach($pendings as $pending)
                                @foreach($transactions as $transaction)
                                @if($pending->id == $transaction->id)
                                @if(count($transaction->vehicles) == 1)
                                <tr>
                                    <th>{{$transaction->request_code}}</th>
                                    @foreach($transaction->vehicles as $wow)
                                    @if($wow->pivot->vehicle_code_number == null)
                                    <td><h6>Not Set</h6></td>
                                    @else
                                    <td><h6>{{$wow->pivot->vehicle_code_number}}</h6></td>
                                    @endif
                                    <td><h6>{{$wow->name}}</h6></td>
                                    <td><h6>{{$wow->pivot->days}}</h6></td>
                                    <td><h6>{{date("F d, Y",strtotime($wow->pivot->date_needed))}}</h6></td>
                                    <td><h6>{{date("F d, Y",strtotime($wow->pivot->return_date))}}</h6></td>
                                    @endforeach
                                    <td>
                                        <h5>
                                        @if($transaction->status->id == 1)
                                        <span class="badge badge-warning">{{$transaction->status->name}}</span>
                                        </h5>
                                        @elseif($transaction->status->id == 2)
                                        <span class="badge badge-primary">{{$transaction->status->name}}</span>
                                        </h5>
                                        @elseif($transaction->status->id == 3)
                                        <span class="badge badge-danger">{{$transaction->status->name}}</span>
                                        </h5>
                                        @elseif($transaction->status->id == 4)
                                        <span class="badge badge-primary">{{$transaction->status->name}}</span>
                                        </h5>
                                        @elseif($transaction->status->id == 5)
                                        <span class="badge badge-secondary">{{$transaction->status->name}}</span>
                                        </h5>
                                        @endif
                                    </td>
                                    @can("isUser")
                                    @if($transaction->status->id == 5)
                                    <td><a href="{{route("transactions.show",["transaction" => $transaction->id])}}" class="btn btn-primary">Show Details</a></td>
                                    @else
                                    <td>
                                        <form action="{{route("transactions.update",["transaction" => $transaction->id])}}?set=cancel" method="post">
                                            @csrf
                                            @method("PUT")
                                            <button class="btn btn-danger">Cancel</button>
                                        </form>
                                    </td>
                                    @endif
                                    
                                    @endcan
                                    @can("isAdmin")
                                    <td>
                                    @if($transaction->status_id == 2)
                                        <form action="{{route("transactions.update",["transaction" => $transaction->id])}}?set=complete" method="post">
                                            @csrf
                                            @method("PUT")
                                            
                                            <button class="btn btn-primary">Complete</button>
                                        </form>
                                    @elseif($transaction->status_id == 3)
                                    <h5><span class="badge badge-danger">{{$transaction->status->name}}</span></h5>
                                    @elseif($transaction->status_id == 4)
                                    @elseif($transaction->status_id == 5)
                                    @else
                                        <form action="{{route("transactions.update",["transaction" => $transaction->id])}}?set=accept" method="post">
                                            @csrf
                                            @method("PUT")
                                        <button class="btn btn-primary mb-1">Accept</button>
                                        </form>
                                        <form action="{{route("transactions.update",["transaction" => $transaction->id])}}?set=reject" method="post">
                                            @csrf
                                            @method("PUT")
                                        <button class="btn btn-danger">Reject</button>
                                        </form>
                                    @endif
                                    </td>
                                    @endcan
                                    <td><h6>{{$transaction->user->name}}</h6></td>


                                </tr>
                                @else
                                <tr>
                                    <th>{{$transaction->request_code}}</th>
                                    <td>
                                    @foreach($transaction->vehicles as $wow)
                                    @if($wow->pivot->vehicle_code_number == null)
                                    <h6>Not Set</h6>
                                    @else
                                    <h6>{{$wow->pivot->vehicle_code_number}}</h6>
                                    @endif
                                    @endforeach
                                    </td>
                                    <td>
                                        @foreach($transaction->vehicles as $wow)
                                        <h6>{{$wow->name}}</h6>
                                        @endforeach
                                    </td>
                                    <td>
                                        @foreach($transaction->vehicles as $wow)
                                        <h6>{{$wow->pivot->days}}</h6>
                                        @endforeach
                                    </td>
                                    <td>
                                        @foreach($transaction->vehicles as $wow)
                                        <h6>{{date("F d, Y",strtotime($wow->pivot->date_needed))}}</h6>
                                        @endforeach
                                    </td>
                                    <td>
                                        @foreach($transaction->vehicles as $wow)
                                        <h6>{{date("F d, Y",strtotime($wow->pivot->return_date))}}</h6>
                                        @endforeach
                                    </td>
                                    <td>
                                        <h5>
                                        @if($transaction->status->id == 1)
                                        <span class="badge badge-warning">{{$transaction->status->name}}</span>
                                        </h5>
                                        @elseif($transaction->status->id == 2)
                                        <span class="badge badge-primary">{{$transaction->status->name}}</span>
                                        </h5>
                                        @elseif($transaction->status->id == 3)
                                        <span class="badge badge-danger">{{$transaction->status->name}}</span>
                                        </h5>
                                        @elseif($transaction->status->id == 4)
                                        <span class="badge badge-primary">{{$transaction->status->name}}</span>
                                        </h5>
                                        @elseif($transaction->status->id == 5)
                                        <span class="badge badge-secondary">{{$transaction->status->name}}</span>
                                        </h5>
                                        @endif
                                    </td>
                                    @can("isUser")
                                    @if($transaction->status->id == 5)
                                    <td><a href="{{route("transactions.show",["transaction" => $transaction->id])}}" class="btn btn-primary">Show Details</a></td>
                                    @else
                                    <td>
                                        <form action="{{route("transactions.update",["transaction" => $transaction->id])}}?set=cancel" method="post">
                                            @csrf
                                            @method("PUT")
                                            <button class="btn btn-danger">Cancel</button>
                                        </form>
                                    </td>
                                    @endif
                                    
                                    @endcan
                                    @can("isAdmin")
                                    <td>
                                    @if($transaction->status_id == 2)
                                        <form action="{{route("transactions.update",["transaction" => $transaction->id])}}?set=complete" method="post">
                                            @csrf
                                            @method("PUT")
                                            
                                            <button class="btn btn-primary">Complete</button>
                                        </form>
                                    @elseif($transaction->status_id == 3)
                                    <h5><span class="badge badge-danger">{{$transaction->status->name}}</span></h5>
                                    @elseif($transaction->status_id == 4)
                                    @elseif($transaction->status_id == 5)
                                    @else
                                        <form action="{{route("transactions.update",["transaction" => $transaction->id])}}?set=accept" method="post">
                                            @csrf
                                            @method("PUT")
                                        <button class="btn btn-primary mb-1">Accept</button>
                                        </form>
                                        <form action="{{route("transactions.update",["transaction" => $transaction->id])}}?set=reject" method="post">
                                            @csrf
                                            @method("PUT")
                                        <button class="btn btn-danger">Reject</button>
                                        </form>
                                    @endif
                                    </td>
                                    @endcan
                                    <td><h6>{{$transaction->user->name}}</h6></td>
                                </tr>
                                @endif
                                @endif
                                @endforeach
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="list-rejected" role="tabpanel" aria-labelledby="list-rejected-list">
            <div class="row">
                <div class="col-12 col-md-8 col-lg-12">
                    {{-- start of table --}}
                    <div class="table-responsive">
                        <table class="table table-striped table-hover text-center">
                            {{-- start of thead --}}
                            <thead>
                                <th>Request Code</th>
                                <th>Asset Code No.</th>
                                <th>Product Name</th>
                                <th>No. of Days</th>
                                <th>Date Needed</th>
                                <th>Return Date</th>
                                <th>Status</th>
                                <th>Action</th>
                                <th>User</th>
                            </thead>
                            {{-- end of thead --}}
                            <tbody>
                                @foreach($rejecteds as $rejected)
                                @foreach($transactions as $transaction)
                                @if($rejected->id == $transaction->id)
                                @if(count($transaction->vehicles) == 1)
                                <tr>
                                    <th>{{$transaction->request_code}}</th>
                                    @foreach($transaction->vehicles as $wow)
                                    @if($wow->pivot->vehicle_code_number == null)
                                    <td><h6>Not Set</h6></td>
                                    @else
                                    <td><h6>{{$wow->pivot->vehicle_code_number}}</h6></td>
                                    @endif
                                    <td><h6>{{$wow->name}}</h6></td>
                                    <td><h6>{{$wow->pivot->days}}</h6></td>
                                    <td><h6>{{date("F d, Y",strtotime($wow->pivot->date_needed))}}</h6></td>
                                    <td><h6>{{date("F d, Y",strtotime($wow->pivot->return_date))}}</h6></td>
                                    @endforeach
                                    <td>
                                        <h5>
                                        @if($transaction->status->id == 1)
                                        <span class="badge badge-warning">{{$transaction->status->name}}</span>
                                        </h5>
                                        @elseif($transaction->status->id == 2)
                                        <span class="badge badge-primary">{{$transaction->status->name}}</span>
                                        </h5>
                                        @elseif($transaction->status->id == 3)
                                        <span class="badge badge-danger">{{$transaction->status->name}}</span>
                                        </h5>
                                        @elseif($transaction->status->id == 4)
                                        <span class="badge badge-primary">{{$transaction->status->name}}</span>
                                        </h5>
                                        @elseif($transaction->status->id == 5)
                                        <span class="badge badge-secondary">{{$transaction->status->name}}</span>
                                        </h5>
                                        @endif
                                    </td>
                                    @can("isUser")
                                    @if($transaction->status->id == 5)
                                    <td><a href="{{route("transactions.show",["transaction" => $transaction->id])}}" class="btn btn-primary">Show Details</a></td>
                                    @else
                                    <td>
                                        <form action="{{route("transactions.update",["transaction" => $transaction->id])}}?set=cancel" method="post">
                                            @csrf
                                            @method("PUT")
                                            <button class="btn btn-danger">Cancel</button>
                                        </form>
                                    </td>
                                    @endif
                                    
                                    @endcan
                                    @can("isAdmin")
                                    <td>
                                    @if($transaction->status_id == 2)
                                        <form action="{{route("transactions.update",["transaction" => $transaction->id])}}?set=complete" method="post">
                                            @csrf
                                            @method("PUT")
                                            
                                            <button class="btn btn-primary">Complete</button>
                                        </form>
                                    @elseif($transaction->status_id == 3)
                                    <h5><span class="badge badge-danger">{{$transaction->status->name}}</span></h5>
                                    @elseif($transaction->status_id == 4)
                                    @elseif($transaction->status_id == 5)
                                    @else
                                        <form action="{{route("transactions.update",["transaction" => $transaction->id])}}?set=accept" method="post">
                                            @csrf
                                            @method("PUT")
                                        <button class="btn btn-primary mb-1">Accept</button>
                                        </form>
                                        <form action="{{route("transactions.update",["transaction" => $transaction->id])}}?set=reject" method="post">
                                            @csrf
                                            @method("PUT")
                                        <button class="btn btn-danger">Reject</button>
                                        </form>
                                    @endif
                                    </td>
                                    @endcan
                                    <td><h6>{{$transaction->user->name}}</h6></td>


                                </tr>
                                @else
                                <tr>
                                    <th>{{$transaction->request_code}}</th>
                                    <td>
                                    @foreach($transaction->vehicles as $wow)
                                    @if($wow->pivot->vehicle_code_number == null)
                                    <h6>Not Set</h6>
                                    @else
                                    <h6>{{$wow->pivot->vehicle_code_number}}</h6>
                                    @endif
                                    @endforeach
                                    </td>
                                    <td>
                                        @foreach($transaction->vehicles as $wow)
                                        <h6>{{$wow->name}}</h6>
                                        @endforeach
                                    </td>
                                    <td>
                                        @foreach($transaction->vehicles as $wow)
                                        <h6>{{$wow->pivot->days}}</h6>
                                        @endforeach
                                    </td>
                                    <td>
                                        @foreach($transaction->vehicles as $wow)
                                        <h6>{{date("F d, Y",strtotime($wow->pivot->date_needed))}}</h6>
                                        @endforeach
                                    </td>
                                    <td>
                                        @foreach($transaction->vehicles as $wow)
                                        <h6>{{date("F d, Y",strtotime($wow->pivot->return_date))}}</h6>
                                        @endforeach
                                    </td>
                                    <td>
                                        <h5>
                                        @if($transaction->status->id == 1)
                                        <span class="badge badge-warning">{{$transaction->status->name}}</span>
                                        </h5>
                                        @elseif($transaction->status->id == 2)
                                        <span class="badge badge-primary">{{$transaction->status->name}}</span>
                                        </h5>
                                        @elseif($transaction->status->id == 3)
                                        <span class="badge badge-danger">{{$transaction->status->name}}</span>
                                        </h5>
                                        @elseif($transaction->status->id == 4)
                                        <span class="badge badge-primary">{{$transaction->status->name}}</span>
                                        </h5>
                                        @elseif($transaction->status->id == 5)
                                        <span class="badge badge-secondary">{{$transaction->status->name}}</span>
                                        </h5>
                                        @endif
                                    </td>
                                    @can("isUser")
                                    @if($transaction->status->id == 5)
                                    <td><a href="{{route("transactions.show",["transaction" => $transaction->id])}}" class="btn btn-primary">Show Details</a></td>
                                    @else
                                    <td>
                                        <form action="{{route("transactions.update",["transaction" => $transaction->id])}}?set=cancel" method="post">
                                            @csrf
                                            @method("PUT")
                                            <button class="btn btn-danger">Cancel</button>
                                        </form>
                                    </td>
                                    @endif
                                    
                                    @endcan
                                    @can("isAdmin")
                                    <td>
                                    @if($transaction->status_id == 2)
                                        <form action="{{route("transactions.update",["transaction" => $transaction->id])}}?set=complete" method="post">
                                            @csrf
                                            @method("PUT")
                                            
                                            <button class="btn btn-primary">Complete</button>
                                        </form>
                                    @elseif($transaction->status_id == 3)
                                    <h5><span class="badge badge-danger">{{$transaction->status->name}}</span></h5>
                                    @elseif($transaction->status_id == 4)
                                    @elseif($transaction->status_id == 5)
                                    @else
                                        <form action="{{route("transactions.update",["transaction" => $transaction->id])}}?set=accept" method="post">
                                            @csrf
                                            @method("PUT")
                                        <button class="btn btn-primary mb-1">Accept</button>
                                        </form>
                                        <form action="{{route("transactions.update",["transaction" => $transaction->id])}}?set=reject" method="post">
                                            @csrf
                                            @method("PUT")
                                        <button class="btn btn-danger">Reject</button>
                                        </form>
                                    @endif
                                    </td>
                                    @endcan
                                    <td><h6>{{$transaction->user->name}}</h6></td>
                                </tr>
                                @endif
                                @endif
                                @endforeach
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="list-completed" role="tabpanel" aria-labelledby="list-completed-list">
            <div class="row">
                <div class="col-12 col-md-8 col-lg-12">
                    {{-- start of table --}}
                    <div class="table-responsive">
                        <table class="table table-striped table-hover text-center">
                            {{-- start of thead --}}
                            <thead>
                                <th>Request Code</th>
                                <th>Asset Code No.</th>
                                <th>Product Name</th>
                                <th>No. of Days</th>
                                <th>Date Needed</th>
                                <th>Return Date</th>
                                <th>Status</th>
                                <th>Action</th>
                                <th>User</th>
                            </thead>
                            {{-- end of thead --}}
                            <tbody>
                                @foreach($completeds as $completed)
                                @foreach($transactions as $transaction)
                                @if($completed->id == $transaction->id)
                                @if(count($transaction->vehicles) == 1)
                                <tr>
                                    <th>{{$transaction->request_code}}</th>
                                    @foreach($transaction->vehicles as $wow)
                                    @if($wow->pivot->vehicle_code_number == null)
                                    <td><h6>Not Set</h6></td>
                                    @else
                                    <td><h6>{{$wow->pivot->vehicle_code_number}}</h6></td>
                                    @endif
                                    <td><h6>{{$wow->name}}</h6></td>
                                    <td><h6>{{$wow->pivot->days}}</h6></td>
                                    <td><h6>{{date("F d, Y",strtotime($wow->pivot->date_needed))}}</h6></td>
                                    <td><h6>{{date("F d, Y",strtotime($wow->pivot->return_date))}}</h6></td>
                                    @endforeach
                                    <td>
                                        <h5>
                                        @if($transaction->status->id == 1)
                                        <span class="badge badge-warning">{{$transaction->status->name}}</span>
                                        </h5>
                                        @elseif($transaction->status->id == 2)
                                        <span class="badge badge-primary">{{$transaction->status->name}}</span>
                                        </h5>
                                        @elseif($transaction->status->id == 3)
                                        <span class="badge badge-danger">{{$transaction->status->name}}</span>
                                        </h5>
                                        @elseif($transaction->status->id == 4)
                                        <span class="badge badge-primary">{{$transaction->status->name}}</span>
                                        </h5>
                                        @elseif($transaction->status->id == 5)
                                        <span class="badge badge-secondary">{{$transaction->status->name}}</span>
                                        </h5>
                                        @endif
                                    </td>
                                    @can("isUser")
                                    @if($transaction->status->id == 5)
                                    <td><a href="{{route("transactions.show",["transaction" => $transaction->id])}}" class="btn btn-primary">Show Details</a></td>
                                    @else
                                    <td>
                                        <form action="{{route("transactions.update",["transaction" => $transaction->id])}}?set=cancel" method="post">
                                            @csrf
                                            @method("PUT")
                                            <button class="btn btn-danger">Cancel</button>
                                        </form>
                                    </td>
                                    @endif
                                    
                                    @endcan
                                    @can("isAdmin")
                                    <td>
                                    @if($transaction->status_id == 2)
                                        <form action="{{route("transactions.update",["transaction" => $transaction->id])}}?set=complete" method="post">
                                            @csrf
                                            @method("PUT")
                                            
                                            <button class="btn btn-primary">Complete</button>
                                        </form>
                                    @elseif($transaction->status_id == 3)
                                    <h5><span class="badge badge-danger">{{$transaction->status->name}}</span></h5>
                                    @elseif($transaction->status_id == 4)
                                    @elseif($transaction->status_id == 5)
                                    @else
                                        <form action="{{route("transactions.update",["transaction" => $transaction->id])}}?set=accept" method="post">
                                            @csrf
                                            @method("PUT")
                                        <button class="btn btn-primary mb-1">Accept</button>
                                        </form>
                                        <form action="{{route("transactions.update",["transaction" => $transaction->id])}}?set=reject" method="post">
                                            @csrf
                                            @method("PUT")
                                        <button class="btn btn-danger">Reject</button>
                                        </form>
                                    @endif
                                    </td>
                                    @endcan
                                    <td><h6>{{$transaction->user->name}}</h6></td>


                                </tr>
                                @else
                                <tr>
                                    <th>{{$transaction->request_code}}</th>
                                    <td>
                                    @foreach($transaction->vehicles as $wow)
                                    @if($wow->pivot->vehicle_code_number == null)
                                    <h6>Not Set</h6>
                                    @else
                                    <h6>{{$wow->pivot->vehicle_code_number}}</h6>
                                    @endif
                                    @endforeach
                                    </td>
                                    <td>
                                        @foreach($transaction->vehicles as $wow)
                                        <h6>{{$wow->name}}</h6>
                                        @endforeach
                                    </td>
                                    <td>
                                        @foreach($transaction->vehicles as $wow)
                                        <h6>{{$wow->pivot->days}}</h6>
                                        @endforeach
                                    </td>
                                    <td>
                                        @foreach($transaction->vehicles as $wow)
                                        <h6>{{date("F d, Y",strtotime($wow->pivot->date_needed))}}</h6>
                                        @endforeach
                                    </td>
                                    <td>
                                        @foreach($transaction->vehicles as $wow)
                                        <h6>{{date("F d, Y",strtotime($wow->pivot->return_date))}}</h6>
                                        @endforeach
                                    </td>
                                    <td>
                                        <h5>
                                        @if($transaction->status->id == 1)
                                        <span class="badge badge-warning">{{$transaction->status->name}}</span>
                                        </h5>
                                        @elseif($transaction->status->id == 2)
                                        <span class="badge badge-primary">{{$transaction->status->name}}</span>
                                        </h5>
                                        @elseif($transaction->status->id == 3)
                                        <span class="badge badge-danger">{{$transaction->status->name}}</span>
                                        </h5>
                                        @elseif($transaction->status->id == 4)
                                        <span class="badge badge-primary">{{$transaction->status->name}}</span>
                                        </h5>
                                        @elseif($transaction->status->id == 5)
                                        <span class="badge badge-secondary">{{$transaction->status->name}}</span>
                                        </h5>
                                        @endif
                                    </td>
                                    @can("isUser")
                                    @if($transaction->status->id == 5)
                                    <td><a href="{{route("transactions.show",["transaction" => $transaction->id])}}" class="btn btn-primary">Show Details</a></td>
                                    @else
                                    <td>
                                        <form action="{{route("transactions.update",["transaction" => $transaction->id])}}?set=cancel" method="post">
                                            @csrf
                                            @method("PUT")
                                            <button class="btn btn-danger">Cancel</button>
                                        </form>
                                    </td>
                                    @endif
                                    
                                    @endcan
                                    @can("isAdmin")
                                    <td>
                                    @if($transaction->status_id == 2)
                                        <form action="{{route("transactions.update",["transaction" => $transaction->id])}}?set=complete" method="post">
                                            @csrf
                                            @method("PUT")
                                            
                                            <button class="btn btn-primary">Complete</button>
                                        </form>
                                    @elseif($transaction->status_id == 3)
                                    <h5><span class="badge badge-danger">{{$transaction->status->name}}</span></h5>
                                    @elseif($transaction->status_id == 4)
                                    @elseif($transaction->status_id == 5)
                                    @else
                                        <form action="{{route("transactions.update",["transaction" => $transaction->id])}}?set=accept" method="post">
                                            @csrf
                                            @method("PUT")
                                        <button class="btn btn-primary mb-1">Accept</button>
                                        </form>
                                        <form action="{{route("transactions.update",["transaction" => $transaction->id])}}?set=reject" method="post">
                                            @csrf
                                            @method("PUT")
                                        <button class="btn btn-danger">Reject</button>
                                        </form>
                                    @endif
                                    </td>
                                    @endcan
                                    <td><h6>{{$transaction->user->name}}</h6></td>
                                </tr>
                                @endif
                                @endif
                                @endforeach
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
        <div class="tab-pane fade" id="list-cancelled" role="tabpanel" aria-labelledby="list-cancelled-list">
            <div class="row">
                <div class="col-12 col-md-8 col-lg-12">
                    {{-- start of table --}}
                    <div class="table-responsive">
                        <table class="table table-striped table-hover text-center">
                            {{-- start of thead --}}
                            <thead>
                                <th>Request Code</th>
                                <th>Asset Code No.</th>
                                <th>Product Name</th>
                                <th>No. of Days</th>
                                <th>Date Needed</th>
                                <th>Return Date</th>
                                <th>Status</th>
                                <th>Action</th>
                                <th>User</th>
                            </thead>
                            {{-- end of thead --}}
                            <tbody>
                                @foreach($cancelleds as $cancelled)
                                @foreach($transactions as $transaction)
                                @if($cancelled->id == $transaction->id)
                                @if(count($transaction->vehicles) == 1)
                                <tr>
                                    <th>{{$transaction->request_code}}</th>
                                    @foreach($transaction->vehicles as $wow)
                                    @if($wow->pivot->vehicle_code_number == null)
                                    <td><h6>Not Set</h6></td>
                                    @else
                                    <td><h6>{{$wow->pivot->vehicle_code_number}}</h6></td>
                                    @endif
                                    <td><h6>{{$wow->name}}</h6></td>
                                    <td><h6>{{$wow->pivot->days}}</h6></td>
                                    <td><h6>{{date("F d, Y",strtotime($wow->pivot->date_needed))}}</h6></td>
                                    <td><h6>{{date("F d, Y",strtotime($wow->pivot->return_date))}}</h6></td>
                                    @endforeach
                                    <td>
                                        <h5>
                                        @if($transaction->status->id == 1)
                                        <span class="badge badge-warning">{{$transaction->status->name}}</span>
                                        </h5>
                                        @elseif($transaction->status->id == 2)
                                        <span class="badge badge-primary">{{$transaction->status->name}}</span>
                                        </h5>
                                        @elseif($transaction->status->id == 3)
                                        <span class="badge badge-danger">{{$transaction->status->name}}</span>
                                        </h5>
                                        @elseif($transaction->status->id == 4)
                                        <span class="badge badge-primary">{{$transaction->status->name}}</span>
                                        </h5>
                                        @elseif($transaction->status->id == 5)
                                        <span class="badge badge-secondary">{{$transaction->status->name}}</span>
                                        </h5>
                                        @endif
                                    </td>
                                    @can("isUser")
                                    @if($transaction->status->id == 5)
                                    <td><a href="{{route("transactions.show",["transaction" => $transaction->id])}}" class="btn btn-primary">Show Details</a></td>
                                    @else
                                    <td>
                                        <form action="{{route("transactions.update",["transaction" => $transaction->id])}}?set=cancel" method="post">
                                            @csrf
                                            @method("PUT")
                                            <button class="btn btn-danger">Cancel</button>
                                        </form>
                                    </td>
                                    @endif
                                    
                                    @endcan
                                    @can("isAdmin")
                                    <td>
                                    @if($transaction->status_id == 2)
                                        <form action="{{route("transactions.update",["transaction" => $transaction->id])}}?set=complete" method="post">
                                            @csrf
                                            @method("PUT")
                                            
                                            <button class="btn btn-primary">Complete</button>
                                        </form>
                                    @elseif($transaction->status_id == 3)
                                    <h5><span class="badge badge-danger">{{$transaction->status->name}}</span></h5>
                                    @elseif($transaction->status_id == 4)
                                    @elseif($transaction->status_id == 5)
                                    @else
                                        <form action="{{route("transactions.update",["transaction" => $transaction->id])}}?set=accept" method="post">
                                            @csrf
                                            @method("PUT")
                                        <button class="btn btn-primary mb-1">Accept</button>
                                        </form>
                                        <form action="{{route("transactions.update",["transaction" => $transaction->id])}}?set=reject" method="post">
                                            @csrf
                                            @method("PUT")
                                        <button class="btn btn-danger">Reject</button>
                                        </form>
                                    @endif
                                    </td>
                                    @endcan
                                    <td><h6>{{$transaction->user->name}}</h6></td>


                                </tr>
                                @else
                                <tr>
                                    <th>{{$transaction->request_code}}</th>
                                    <td>
                                    @foreach($transaction->vehicles as $wow)
                                    @if($wow->pivot->vehicle_code_number == null)
                                    <h6>Not Set</h6>
                                    @else
                                    <h6>{{$wow->pivot->vehicle_code_number}}</h6>
                                    @endif
                                    @endforeach
                                    </td>
                                    <td>
                                        @foreach($transaction->vehicles as $wow)
                                        <h6>{{$wow->name}}</h6>
                                        @endforeach
                                    </td>
                                    <td>
                                        @foreach($transaction->vehicles as $wow)
                                        <h6>{{$wow->pivot->days}}</h6>
                                        @endforeach
                                    </td>
                                    <td>
                                        @foreach($transaction->vehicles as $wow)
                                        <h6>{{date("F d, Y",strtotime($wow->pivot->date_needed))}}</h6>
                                        @endforeach
                                    </td>
                                    <td>
                                        @foreach($transaction->vehicles as $wow)
                                        <h6>{{date("F d, Y",strtotime($wow->pivot->return_date))}}</h6>
                                        @endforeach
                                    </td>
                                    <td>
                                        <h5>
                                        @if($transaction->status->id == 1)
                                        <span class="badge badge-warning">{{$transaction->status->name}}</span>
                                        </h5>
                                        @elseif($transaction->status->id == 2)
                                        <span class="badge badge-primary">{{$transaction->status->name}}</span>
                                        </h5>
                                        @elseif($transaction->status->id == 3)
                                        <span class="badge badge-danger">{{$transaction->status->name}}</span>
                                        </h5>
                                        @elseif($transaction->status->id == 4)
                                        <span class="badge badge-primary">{{$transaction->status->name}}</span>
                                        </h5>
                                        @elseif($transaction->status->id == 5)
                                        <span class="badge badge-secondary">{{$transaction->status->name}}</span>
                                        </h5>
                                        @endif
                                    </td>
                                    @can("isUser")
                                    @if($transaction->status->id == 5)
                                    <td><a href="{{route("transactions.show",["transaction" => $transaction->id])}}" class="btn btn-primary">Show Details</a></td>
                                    @else
                                    <td>
                                        <form action="{{route("transactions.update",["transaction" => $transaction->id])}}?set=cancel" method="post">
                                            @csrf
                                            @method("PUT")
                                            <button class="btn btn-danger">Cancel</button>
                                        </form>
                                    </td>
                                    @endif
                                    
                                    @endcan
                                    @can("isAdmin")
                                    <td>
                                    @if($transaction->status_id == 2)
                                        <form action="{{route("transactions.update",["transaction" => $transaction->id])}}?set=complete" method="post">
                                            @csrf
                                            @method("PUT")
                                            
                                            <button class="btn btn-primary">Complete</button>
                                        </form>
                                    @elseif($transaction->status_id == 3)
                                    <h5><span class="badge badge-danger">{{$transaction->status->name}}</span></h5>
                                    @elseif($transaction->status_id == 4)
                                    @elseif($transaction->status_id == 5)
                                    @else
                                        <form action="{{route("transactions.update",["transaction" => $transaction->id])}}?set=accept" method="post">
                                            @csrf
                                            @method("PUT")
                                        <button class="btn btn-primary mb-1">Accept</button>
                                        </form>
                                        <form action="{{route("transactions.update",["transaction" => $transaction->id])}}?set=reject" method="post">
                                            @csrf
                                            @method("PUT")
                                        <button class="btn btn-danger">Reject</button>
                                        </form>
                                    @endif
                                    </td>
                                    @endcan
                                    <td><h6>{{$transaction->user->name}}</h6></td>
                                </tr>
                                @endif
                                @endif
                                @endforeach
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            
        </div>
    </div>


</div>

@endsection