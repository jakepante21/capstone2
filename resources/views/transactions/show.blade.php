@extends("layouts.app")
@section("content")
	<div class="container">
		<div class="jumbtron">
			<h2 class="text-center">Haystzzzz</h2>
		</div>
		<div class="row">
			<div class="col-12">
				{{-- start of table --}}
				<div class="table-responsive">
					{{-- start of transaction table --}}
					<table class="table table-striped">
						<tbody>
							<tr>
								<td>Customer Name:</td>
								<td>{{$transaction->user->name}}</td>
							</tr>
							<tr>
								<td>Transaction Code:</td>
								<td>{{$transaction->request_code}}</td>
							</tr>
							<tr>
								<td>Payment Mode:</td>
								<td>{{$transaction->payment_mode->name}}</td>
							</tr>
							<tr>
								<td>Date of Purchase</td>
								<td>{{$transaction->created_at->format("F d, Y")}}</td>
							</tr>
							<tr>
								<td>Status:</td>
								<td>
									@can("isUser")
									<span class="badge badge-warning">{{$transaction->status->name}}</span>
									@endcan
									@can("isAdmin")
									<form action="{{route("transactions.update",["transaction" => $transaction->id])}}" method="post">
										@csrf
										@method("PUT")
										
										{{-- <span class="badge badge-warning">{{$transaction->status->name}}</span> --}}

										<select name="status" id="status" class="form-control w-25 mb-1">
											@foreach($statuses as $status)
											<option value="{{$status->id}}" class="form-control" {{$transaction->status_id == $status->id ? "selected" : ""}}>{{$status->name}}</option>
											@endforeach
										</select>
										<button class="btn btn-outline-warning w-25">Edit</button>
										
									</form>
									@endcan
								</td>
							</tr>
						</tbody>
					</table>
					{{-- end of transaction table --}}

					{{-- start of table product_transaction --}}
					<table class="table table-striped table-hover">
						<thead>
							<th scope="row">Product Name:</th>
							<th scope="row">Price:</th>
							<th scope="row">No. of Days:</th>
							<th scope="row">Total:</th>
						</thead>
						<tbody>
							{{-- start of product transaction details --}}
							@foreach($transaction->vehicles as $vehicle)
							<tr>
								<td>{{$vehicle->name}}</td>
								<td>&#8369;<span>{{number_format($vehicle->price,2)}}</span></td>
								<td>{{$vehicle->pivot->days}}</td>
								<td>&#8369;<span>{{number_format($vehicle->pivot->subtotal,2)}}</span></td>
							</tr>
							@endforeach
							{{-- end of product transaction details --}}
						</tbody>
						<tfoot>
							<tr>
								<td colspan="3" class="text-right"><strong>Total:</strong></td>
								<td>&#8369;<span>{{number_format($transaction->total,2)}}</span></td>
							</tr>
						</tfoot>
					</table>
					{{-- end of table product_transaction --}}
				</div>
				{{-- end of table --}}
			</div>
		</div>
	</div>
@endsection