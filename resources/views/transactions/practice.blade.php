@extends("layouts.app")
@section("content")
    <div class="container-fluid">
        <div class="jumbotron">
            <h2 class="text-center">Requests</h2>
        </div>
        <div class="row mb-2">
            <div class="col-12 col-md-8 col-lg-6">
                <form>
                    <button class="btn btn-secondary">All</button>
                    <button class="btn btn-secondary">Accepted</button>
                    <button class="btn btn-secondary">Pending</button>
                    <button class="btn btn-secondary">Completed</button>
                    <button class="btn btn-secondary">Rejected</button>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-8 col-lg-12">
                {{-- start of table --}}
                <div class="table-responsive">
                    <table class="table table-striped table-hover text-center">
                        {{-- start of thead --}}
                        <thead>
                            <th>Request Code</th>
                            <th>Asset Code No.</th>
                            <th>Product Name</th>
                            <th>No. of Days</th>
                            <th>Date Needed</th>
                            <th>Return Date</th>
                            <th>Status</th>
                            <th>Action</th>
                            <th>User</th>
                        </thead>
                        {{-- end of thead --}}
                        <tbody>
                            @foreach($transactions as $transaction)
                            @foreach($assettransactions as $assetts)
                            @if($transaction->id == $assetts->transaction_id)
                            <tr>
                                <th>{{$transaction->request_code}}</th>
                                
                                
                                @if($assetts->vehicle_code_number == null)
                                <td>Not Set</td>
                                @else
                                <td>{{$assetts->vehicle_code_number}}</td>
                                @endif
                                <td>{{$assetts->product->name}}</td>
                                <td>{{$assetts->days}}</td>
                                <td>{{date("F d, Y",strtotime($assetts->date_needed))}}</td>
                                <td>{{date("F d, Y",strtotime($assetts->return_date))}}</td>
                                
                                <td><h5>
                                    @if($transaction->status->id == 1)
                                    <span class="badge badge-warning">{{$transaction->status->name}}</span>
                                    </h5>
                                    @elseif($transaction->status->id == 2)
                                    <span class="badge badge-primary">{{$transaction->status->name}}</span>
                                    </h5>
                                    @elseif($transaction->status->id == 3)
                                    <span class="badge badge-danger">{{$transaction->status->name}}</span>
                                    </h5>
                                    @elseif($transaction->status->id == 4)
                                    <span class="badge badge-primary">{{$transaction->status->name}}</span>
                                    </h5>
                                    @endif
                                </td>
                                
                                <td>
                                    @if($transaction->status_id == 2)
                                    <form action="{{route("transactions.action",["asset" => $assetts->id])}}?set=complete" method="post">
                                        @csrf
                                        @method("PUT")
                                        <input type="hidden" name="assett" value="{{$assetts->vehicle_code_number}}">
                                        <button class="btn btn-primary">Complete</button>
                                    </form>
                                    @elseif($transaction->status_id == 3)
                                    <h5><span class="badge badge-danger">{{$transaction->status->name}}</span></h5>
                                    @elseif($transaction->status_id == 4)
                                    @else
                                    <form action="{{route("transactions.action",["asset" => $assetts->id])}}?set=accept" method="post">
                                        @csrf
                                        @method("PUT")
                                        <button class="btn btn-primary mb-1">Accept</button>
                                    </form>
                                    <form action="{{route("transactions.action",["asset" => $assetts->id])}}?set=reject" method="post">
                                        @csrf
                                        @method("PUT")
                                        <button class="btn btn-danger">Reject</button>
                                    </form>
                                    @endif
                                </td>
                                <td>{{$transaction->user->name}}</td>
                                
                                
                            </tr>
                            @endif
                            @endforeach
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection