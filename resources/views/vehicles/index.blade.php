@extends("layouts.app")

@section("content")
	<div class="container">
		<div class="jumbotron">
			<h2 class="text-center">Vehicle Total Stocks: {{$stock}}</h2>
		</div>
		
			
			@foreach($products as $product)
			<div class="accordion" id="accordionExample">
				<div class="card">
					<div class="card-header" id="headingOne">
						<div class="row">
							<div class="col-12 col-md-8- col-lg-4">
								<h2 class="mb-0">
									<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne{{$product->id}}" aria-expanded="true" aria-controls="collapseOne">
										<h2>{{$product->name}}</h2> 
									</button>
								</h2>
							</div>
							<div class="col-12 col-md-8 col-lg-8">
								<h6>No. of Stock: {{$product->stocks}}</h6>
								<h6>No. of Available Stock: {{$product->stocks_avail}}</h6>
								<h6>No. of Not Available Stock: {{$product->stocks_not_avail}}</h6>
							</div>
						</div>
						
					</div>

					<div id="collapseOne{{$product->id}}" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
						<div class="card-body">
							<div class="col-12">
								<div class="table-responsive">
									<table class="table table-striped">
										<tbody>
											@foreach($vehicles as $vehicle)
											@if($product->id == $vehicle->product_id)
											<tr>
												<td>Plate No.:</td>
												<td>{{$vehicle->plate_number}}</td>
												<td>Code No.:</td>
												<td>{{$vehicle->asset_code_number}}</td>
												<td>
													<form action="{{route("vehicles.update",["vehicle" => $vehicle->id])}}" method="post">
														@csrf
														@method("PUT")
														<select name="status" class="form-control mb-1">
															@foreach($statuses as $status)
															<option value="{{$status->id}}" {{$vehicle->asset_status_id == $status->id ? "selected" : ""}}>{{$status->name}}</option>
															@endforeach
														</select>
														<button class="btn btn-warning w-100">Set Status</button>
													</form>
												</td>
												<td>
													<form action="{{route("vehicles.destroy",["vehicle" => $vehicle->id])}}" method="post">
														@csrf
														@method("DELETE")
														<button class="btn btn-danger">Remove Vehicle</button>
													</form>
												</td>
											</tr>
											@else
											@endif
											@endforeach
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			@endforeach
			
			{{-- <div class="col-12 col-md-8 col-lg-3">
				
			</div> --}}
			
		
	</div>
@endsection

