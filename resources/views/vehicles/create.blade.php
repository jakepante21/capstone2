@extends("layouts.app")
@section("content")
	<div class="container">
		<div class="jumbotron">
			<h2 class="text-center">Add Stock</h2>
		</div>
		<div class="row">
			<div class="col-12 col-md-8 col-lg-4 mx-auto">
				<div class="card" style="width: 18rem;">
					<img src="/public/{{$product->image}}" class="card-img-top" alt="...">
					<div class="card-body">
					<h5 class="card-title">{{$product->name}}</h5>
					<p class="card-text">&#8369;{{number_format($product->price,2)}}</p>
					<p class="card-text">{{$product->description}}</p>
					<p class="card-text"></p>
							@foreach($categories as $category)
							<p class="card-text">{{$product->category_id == $category->id ? "$category->name" : ""}}</p>
							@endforeach
				</div>
				<div class="card-footer">
					<form action="{{route("vehicles.store")}}" method="post">
						@csrf
						{{-- <input type="number" name="quantity" id="quantity" class="form-control mb-1" placeholder="quantity" min="1"> --}}
						<input type="text" name="plate-number" id="plate-number" class="form-control mb-1" placeholder="Plate Number">
						<button type="submit" class="btn btn-primary w-100">Add Stock</button>
					</form>
				</div>

				</div>
			</div>
		</div>
	</div>
@endsection