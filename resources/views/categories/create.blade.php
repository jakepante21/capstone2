@extends("layouts.app")

@section("content")
	<div class="container">
		<div class="jumbotron">
			<h2 class="text-center">Create Category</h2>
		</div>
		<div class="row">
			<div class="col-12 col-md-8 mx-auto">
				<form action="{{route("categories.store")}}" method="post">
					@csrf
					<input type="text" name="name" id="name" class="form-control mb-1" placeholder="Category Name">
					<button type="submit" class="btn btn-primary mb-1 w-100">Create Category</button>
				</form>
			</div>
		</div>
	</div>
@endsection