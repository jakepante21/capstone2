@extends("layouts.app")
@section("content")

	<h4><a href="{{route("categories.index")}}">View All Categories</a></h4>

	@if($category)	
		<h1>{{$category->name}}</h1>
		<p>Updated At: {{$category->updated_at}}</p>
	@else
		<h1>No Record with that ID</h1>
	@endif

@endsection