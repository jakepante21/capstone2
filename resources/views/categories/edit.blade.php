@extends("layouts.app")
@section("content")
	<div class="container">
		<div class="jumbotron">
			<h2 class="text-center">Edit Category</h2>
		</div>
		<form action="{{route("categories.update",["category" => $category->id])}}" method="post">
			@method("PUT")
			@csrf
			<label for="name">Category Name:</label>
			<input type="text" name="name" id="name" value="{{$category->name}}">
			<button type="submit" class="btn btn-warning">Update Category</button>
		</form>
	</div>
@endsection