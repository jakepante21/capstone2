@extends("layouts/app")
@section("content")

	<div class="container">
		<div class="row">
			<div class="col-12">
				<h3>My Request Cart</h3>
			</div>
			@if(Session::has("status"))
			<div class="col-12">
				<div class="alert alert-warning">
					{{Session::get("status")}}
				</div>
			</div>
			@endif
			{{-- @include("products.includes.error-status") --}}
			@if(Session::has("cart"))
			<div class="col-12">
				{{-- table --}}
				<div class="table-responsive">
					<table class="table table-striped table-hover text-center">
						<thead>
							<th scope="col">Name</th>
							<th scope="col">Rent Price Per Day</th>
							<th scope="col">No. of Days</th>
							<th scope="col">Date Needed</th>
							<th scope="col">Return Date</th>
							<th scope="col">Subtotal</th>
							<th scope="col">Action</th>
						</thead>
						<tbody>
							{{-- start of row --}}
							@foreach($products as $product)
							<tr>
								<th scope="row">{{$product->name}}</th>
								<td>&#8369;<span>{{number_format($product->price,2)}}</span></td>
								<form action="{{route("carts.update",["cart"=>$product->id])}}" method="post">
								<td>
									<div class="add-tocart-field mb-1">
										
											@csrf
											@method("PUT")
											<input type="number" name="days" value="{{$product->days}}" class="p-1 w-50" min="1" max="5">
											<button type="submit" class="btn btn-outline-secondary">Edit</button>
										
									</div>
								</td>
								<td>
									
										<input type="date" name="date" id="date" class="form-control" value="{{$product->date}}">
									
								</td>
								</form>
								<td>
									{{date("F d, Y", strtotime($product->returndate))}}
								</td>
								<td>&#8369;<span>{{number_format($product->subtotal,2)}}</span></td>
								<td>
									<form action="{{route("carts.destroy",["cart" => $product->id])}}" method="post">
										@method("DELETE")
										@csrf
										<button class="btn btn-danger w-100">Remove</button>
									</form>
								</td>
							</tr>
							@endforeach
							{{-- end of row --}}
						</tbody>
						<tfoot>
							<tr>
								<td colspan="4" class="text-right">Total</td>
								<td>&#8369;<span id="total">{{number_format($total,2)}}</span></td>
								<td>
									{{-- @can("isLogged") --}}
									<form action="{{route("transactions.store")}}" method="post">
										@csrf
										<div id="paypal-btn"></div>
										<button class="btn btn-primary w-100" type="submit">Submit Request</button>
									</form>
									{{-- @endcan
									@cannot("isLogged")
										<a href="{{route("login")}}" class="btn btn-success">Log In first</a>
									@endcan --}}
								</td>
							</tr>
						</tfoot>
					</table>
				</div>
				{{-- end of table --}}
				<form action="{{route("carts.empty")}}" method="post">
					@csrf
					@method("DELETE")
					<button type="submit" class="btn btn-outline-danger">Clear Request Cart</button>
				</form>
			</div>
			@else
				<div class="col-12">
					<div class="alert alert-info">
						Request Cart is Empty!
					</div>
				</div>
			@endif
		</div>
	</div>
	<script src="https://www.paypal.com/sdk/js?client-id=AZAifrojZxAvu0z3bgFtBfqzmHJHzWQ5jeP_lrlzEeIzUYYhIcck-hXgM8nVa5qoVPDDQ-CZttR1jTH6"></script>
@endsection