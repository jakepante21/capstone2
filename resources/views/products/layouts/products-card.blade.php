<div class="card" style="width: 18rem;">
	<img src="/public/{{$product->image}}" class="card-img-top" alt="...">
	<div class="card-body">
		<h5 class="card-title">{{$product->name}}</h5>
		<p class="card-text">&#8369;{{number_format($product->price,2)}} Per day <span class="badge badge-{{$product->stock_status_id == 1 ? "primary" :"secondary"}}">{{$product->stock_status->name}}</span></p>
		<p class="card-text">{{$product->description}}</p>
		<p class="card-text">{{$product->category->name}}</p>
							{{-- @foreach($categories as $category)
							<p class="card-text">{{$product->category_id == $category->id ? "$category->name" : ""}}</p>
							@endforeach --}}
						</div>
						<div class="card-footer">
							@cannot("isAdmin")
							<form action="{{route("carts.update",["cart" => $product->id])}}" method="post">
								@method("PUT")
								@csrf
								<?php
								$mytime = date("Y-m-d")
								?>
								<label for="date">Date Needed:</label>
								<input type="date" name="date" id="date" class="form-control mb-1" min="{{$mytime}}">
								<label for="days">Rent for Number of Days:</label>
								<input type="number" name="days" id="days" class="form-control mb-1" min="1" value="1" max="5">
								@if($product->stock_status_id === 1)
								<button class="btn btn-dark w-100 mb-1">Rent this Vehicle</button>
								@else
								<button class="btn w-100 mb-1" disabled>Rent this Vehicle</button>
								@endif
								
							</form>
							@endcannot
							<a href="{{route("products.show",["product" => $product->id])}}" class="btn btn-secondary w-100 mb-1">View Product</a>
							@can("isAdmin")
							<a href="{{route("products.edit",["product" => $product->id])}}" class="btn btn-outline-warning w-100 mb-1">Edit Product</a>
							<form action="{{route("products.destroy",["product" => $product->id])}}" method="post">
								@csrf
								@method("DELETE")
								<button class="btn btn-outline-danger w-100 mb-1">Delete Product</button>
								
							</form>
							{{-- <form action="{{route("vehicles.pasa",["vehicle" => $product->id])}}" method="post">
								@csrf
								
								<button class="btn btn-secondary w-100">Add a stock</button>
							</form> --}}
							<a href="{{route("vehicles.pass",["vehicle" => $product->id])}}" class="btn btn-secondary w-100">Add a Stock</a>
							@endcan
						</div>
					</div>