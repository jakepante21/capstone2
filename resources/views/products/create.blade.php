@extends("layouts.app")

@section("content")
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-8 mx-auto">
				<div class="jumbotron">
					<h3 class="text-center">Create Product</h3>
				</div>
				
				<hr>
				<form action="{{route("products.store")}}" method="post" enctype="multipart/form-data">
					@csrf
					@if($errors->has("name"))
						<div class="alert alert-danger">
							{{$errors->first("name")}}
						</div>
					@endif
					<input type="text" name="name" id="name" class="form-control mb-1" placeholder="product name" value="{{old("name")}}">
					@if($errors->has("price"))
						<div class="alert alert-danger">
							{{$errors->first("price")}}
						</div>
					@endif
					<input type="number" name="price" id="price" class="form-control mb-1" placeholder="product price" value="{{old("price")}}">
					@if($errors->has("image"))
						<div class="alert alert-danger">
							{{$errors->first("image")}}
						</div>
					@endif
					<input type="file" name="image" id="image" class="form-control-file mb-1">
					@if($errors->has("category-id"))
						<div class="alert alert-danger">
							{{$errors->first("category-id")}}
						</div>
					@endif
					<select class="custom-select mb-1" name="category-id" id="category-id">
						{{-- <option disabled="true" selected>Categories</option>	 --}}
						@foreach($categories as $category)
						<option value="{{$category->id}}" {{old("category-id") == $category->id ? "selected" : ""}}>{{$category->name}}</option>
						@endforeach
						
					</select>
					@if($errors->has("description"))
						<div class="alert alert-danger">
							{{$errors->first("description")}}
						</div>
					@endif
					<textarea class="form-control mb-3" name="description" id="description" cols="10" rows="10" placeholder="Product Description">{{old("description")}}</textarea>
					<button type="submit" class="btn btn-primary mb-1">Create New Product</button>
				</form>
			</div>
		</div>
	</div>
@endsection