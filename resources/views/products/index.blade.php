@extends("layouts.app")
@section("content")
	<div class="container">
		<div class="jumbotron">
			<h1 class="text-center">Here at Rentacar, You can rent a car!</h1>
		</div>
		<div class="row">
			<div class="col-12">
				<form action="" method="get">	
					<div class="row">
						<div class="col">
							<select name="category" id="category" class="form-control m-2">
								<option value="">ALL</option>
								@foreach($categories as $category)
								<option value="{{ $category->id }}">{{ $category->name }}</option>
								@endforeach
							</select>
						</div>
						<button class="btn btn-outline-primary-sm">Filter</button>
					</div>
				</form>
			</div>
		</div>
		
		<div class="row">
		@foreach($products as $product)
			<div class="col-12 col-md-8 col-lg-3 ">

				@include("products.layouts.products-card")
					
			</div>
			@endforeach
		</div>

		
	</div>
@endsection