@extends("layouts.app")
@section("content")
	<div class="container">
		<div class="jumbotron">
			<h2 class="text-center">View Product</h2>
		</div>
		<div class="row">
			<div class="col-12 mx-auto">
				@include("products.layouts.products-card")
			</div>
		</div>
	</div>
@endsection