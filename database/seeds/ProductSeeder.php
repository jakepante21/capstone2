<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("products")->insert([
        	"name" => "Mirage",
        	"price" => "3000.00",
        	"description" => "This new Mirage 2019 is the bomb. It can serve up to 5 persons or a small family. Go rent it!",
        	"image" => "public/zilFbfDm3dlH37WeJqB3XEh1lUibl0eO1OH4P3tK.png",
        	"category_id" => 1,
            "stock_status_id" => 1,
            "stocks" => 0,
            "stocks_avail" => 0,
            "stocks_not_avail" => 0
        ]);
        DB::table("products")->insert([
        	"name" => "Montero",
        	"price" => "5000.00",
        	"description" => "This new Mirage 2019 is the bomb. It can serve up to 5 persons or a small family. Go rent it!",
        	"image" => "public/CcHtYAfdvMAWNEv365320K8OGsoKxS5RVAkIUqyi.jpeg",
        	"category_id" => 2,
            "stock_status_id" => 1,
            "stocks" => 0,
            "stocks_avail" => 0,
            "stocks_not_avail" => 0
        ]);
    }
}
