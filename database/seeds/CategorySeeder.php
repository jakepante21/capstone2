<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("categories")->insert([
        	"name" => "Cars"
        ]);
        DB::table("categories")->insert([
        	"name" => "SUV"
        ]);
        DB::table("categories")->insert([
        	"name" => "MPV"
        ]);
        DB::table("categories")->insert([
        	"name" => "Pickup Truck"
        ]);
        DB::table("categories")->insert([
        	"name" => "Utility"
        ]);
        DB::table("categories")->insert([
        	"name" => "Van"
        ]);
    }
}
