<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            CategorySeeder::class,
            StockStatusSeeder::class,
            ProductSeeder::class,
     		AssetStatusSeeder::class,
            RoleSeeder::class,
            RequestStatusSeeder::class,
            UserSeeder::class,
            PaymentModeSeeder::class
        ]);
    }
}
