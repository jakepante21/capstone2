<?php

use Illuminate\Database\Seeder;

class StockStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("stock_statuses")->insert([
        	"name" => "Available"
        ]);
        DB::table("stock_statuses")->insert([
        	"name" => "Not Available"
        ]);
        DB::table("stock_statuses")->insert([
        	"name" => "Out of Stock"
        ]);
    }
}
