<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("plate_number")->unique();
            $table->string("asset_code_number");
            $table->unsignedBigInteger("product_id")->nullable();
            $table->foreign("product_id")
                ->references("id")->on("products")
                ->onDelete("set null")
                ->onUpdate("set null");
            $table->unsignedBigInteger("asset_status_id")->nullable()->default(1);
            $table->foreign("asset_status_id")
                ->references("id")->on("asset_statuses")
                ->onDelete("set null")
                ->onUpdate("set null");
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicles');
    }
}
